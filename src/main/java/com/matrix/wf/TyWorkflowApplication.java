package com.matrix.wf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author matrix
 */
@SpringBootApplication
public class TyWorkflowApplication {

    public static void main(String[] args) {
        SpringApplication.run(TyWorkflowApplication.class, args);
    }

}
