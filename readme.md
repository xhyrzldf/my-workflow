
# 软件部开发流程

## 总体流程

- 本地检查(check-style)
- 服务端检查(check-style)
- 自动化单元测试(reporter)
- code-review(确认后合并)
- 集成测试(自动化?手动合并?)
- 自动部署
